package com.doston.alibarakaseller.phone_number

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.doston.alibarakaseller.auth.verification.VerificationCodeFragmentDirections
import com.doston.alibarakaseller.base.BaseFragment
import com.doston.alibarakaseller.databinding.FragmentPhoneNumberBinding
import com.doston.alibarakaseller.helper.MaskWatcher
import com.jakewharton.rxbinding.widget.RxTextView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhoneNumberFragment :
    BaseFragment<FragmentPhoneNumberBinding>(FragmentPhoneNumberBinding::inflate) {



    override fun setUpItems() {
        binding?.phoneNumberPhoneNumber?.addTextChangedListener(MaskWatcher("## ### ## ##"))

        RxTextView.textChanges(binding.phoneNumberPhoneNumber)
            .subscribe {
                binding.phoneNumberConfirmBtn.isEnabled = it.length >= 12
            }
    }

    override fun setUpViews() {
        binding.phoneNumberConfirmBtn.setOnClickListener {
            getBaseActivity {
                it.navController?.navigate(
                    PhoneNumberFragmentDirections.actionPhoneNumberFragmentToVerificationCodeFragment()
                )
            }
        }
    }
}