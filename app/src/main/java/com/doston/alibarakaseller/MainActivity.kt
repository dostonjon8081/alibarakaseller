package com.doston.alibarakaseller

import android.util.Log
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import com.doston.alibarakaseller.base.BaseActivity
import com.doston.alibarakaseller.databinding.ActivityMainBinding
import com.doston.alibarakaseller.helper.USER_FIRST_NAME
import com.doston.alibarakaseller.helper.dataStore
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {

    lateinit var pref: Preferences
    private var name: String? = ""
//    val USER_FIRST_NAME = stringPreferencesKey("user_first_name")


    override fun setupItems() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController


//        Log.d("prefs", "setupItems: ${names.toString()}")
        lifecycleScope.launch {

            pref = this@MainActivity.dataStore.data.first()

            Log.d("prefs", "setupItems1: ${pref[USER_FIRST_NAME]}")
            name = pref[USER_FIRST_NAME]

        }

        Log.d("prefs", "setupItems2: ${name ?: ""}")
    }

}