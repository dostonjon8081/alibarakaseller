package com.doston.alibarakaseller.auth.registeration

import com.doston.alibarakaseller.base.BaseFragment
import com.doston.alibarakaseller.databinding.FragmentRegisterShopBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterShopFragment:BaseFragment<FragmentRegisterShopBinding>(FragmentRegisterShopBinding::inflate) {
    override fun setUpItems() {
    }

    override fun setUpViews() {

    }
}