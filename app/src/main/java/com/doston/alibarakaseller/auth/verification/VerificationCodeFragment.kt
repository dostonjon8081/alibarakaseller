package com.doston.alibarakaseller.auth.verification

import com.doston.alibarakaseller.base.BaseFragment
import com.doston.alibarakaseller.databinding.FragmentVerificationCodeBinding
import com.doston.alibarakaseller.helper.OTPGenericTextWatcher
import com.jakewharton.rxbinding.widget.RxTextView
import dagger.hilt.android.AndroidEntryPoint
import rx.Observable

@AndroidEntryPoint
class VerificationCodeFragment :
    BaseFragment<FragmentVerificationCodeBinding>(FragmentVerificationCodeBinding::inflate) {

    override fun setUpItems() {
        initTextWatcher()
        initRxTextWatcher()

    }

    private fun initRxTextWatcher() {
        val ob1 = RxTextView.textChanges(binding.verificationCodeCode1)
        val ob2 = RxTextView.textChanges(binding.verificationCodeCode2)
        val ob3 = RxTextView.textChanges(binding.verificationCodeCode3)
        val ob4 = RxTextView.textChanges(binding.verificationCodeCode4)

        Observable.combineLatest(ob1, ob2, ob3, ob4) { o1, o2, o3, o4 ->
            (o1.isNotEmpty()
                    && o2.isNotEmpty()
                    && o3.isNotEmpty()
                    && o4.isNotEmpty())
        }.subscribe {
            binding.verificationCodeConfirmBtn.isEnabled = it
        }
    }

    override fun setUpViews() {

    }


    private fun initTextWatcher() {
        binding.verificationCodeCode1.addTextChangedListener(
            OTPGenericTextWatcher(binding.verificationCodeCode2, binding.verificationCodeCode1)
        )

        binding.verificationCodeCode2.addTextChangedListener(
            OTPGenericTextWatcher(binding.verificationCodeCode3, binding.verificationCodeCode1)
        )

        binding.verificationCodeCode3.addTextChangedListener(
            OTPGenericTextWatcher(binding.verificationCodeCode4, binding.verificationCodeCode2)
        )

        binding.verificationCodeCode4.addTextChangedListener(
            OTPGenericTextWatcher(binding.verificationCodeCode4, binding.verificationCodeCode3)
        )
    }
}
