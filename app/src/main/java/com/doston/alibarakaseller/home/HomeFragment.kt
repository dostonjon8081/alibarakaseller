package com.doston.alibarakaseller.home

import android.content.Context
import android.os.Bundle
import android.view.View
import com.doston.alibarakaseller.base.BaseFragment
import com.doston.alibarakaseller.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment:BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {
    override fun setUpItems() {
    }

    override fun setUpViews() {

    }
}