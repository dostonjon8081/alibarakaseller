package com.doston.alibarakaseller.helper

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore

fun Context.ac(){

}

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "account")

val USER_FIRST_NAME = stringPreferencesKey("user_first_name")

