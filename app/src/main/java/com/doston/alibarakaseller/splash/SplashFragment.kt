package com.doston.alibarakaseller.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import com.doston.alibarakaseller.base.BaseFragment
import com.doston.alibarakaseller.databinding.FragmentSplashBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {

    override fun setUpItems() {
    }

    override fun setUpViews() {
        Handler(Looper.myLooper()!!).postDelayed({
            getBaseActivity {
                it.navController?.navigate(SplashFragmentDirections.actionSplashToPhoneNumberFragment())
            }
        }, 2000)
    }
}