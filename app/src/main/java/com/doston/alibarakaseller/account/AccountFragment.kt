package com.doston.alibarakaseller.account

import com.doston.alibarakaseller.base.BaseFragment
import com.doston.alibarakaseller.databinding.FragmentAccountBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AccountFragment : BaseFragment<FragmentAccountBinding>(FragmentAccountBinding::inflate) {
    override fun setUpItems() {
    }

    override fun setUpViews() {

    }
}